import { FlightsModel, Flight, FlightStatus } from '../models/flights.model'
import { PersonsModel } from '../models/persons.model'
import { HttpException, HttpStatus } from '../index'

export class FlightsService {
    getAll() {
        return FlightsModel.find({})
    }

    async onboardPassenger(dto: OnboardPassengerDto): Promise<Flight> {
        console.log(dto)
        const flight = await FlightsModel.findById(dto.flightId)

        if (!flight) {
            throw new HttpException('Flight not found.', HttpStatus.NotFound)
        }

        if (flight.status !== FlightStatus.Boarding) {
            throw new HttpException(
                `Cannot board a passenger when a flight is in status "${flight.status}".`,
                HttpStatus.BadRequest
            )
        }

        const user = await PersonsModel.findById(dto.passengerId)

        if (!user) {
            throw new HttpException('User not found.', HttpStatus.NotFound)
        }

        flight.passengers.push(dto.passengerId)
        await flight.save()
        return flight
    }
}

export type OnboardPassengerDto = {
    flightId: string
    passengerId: string
}
