import mongoose, { Schema } from 'mongoose'

export interface Flight {
    code: string
    origin: string
    destination: string
    passengers: string[]
    status: FlightStatus
}

export enum FlightStatus {
    Pending = 'pending',
    Boarding = 'boarding',
    Departing = 'departing',
    InFlight = 'in-flight',
}

const schema = new Schema<Flight>(
    {
        code: { required: true, type: String },
        origin: { required: true, type: String },
        destination: { required: true, type: String },
        passengers: { type: Array },
        status: {
            type: String,
            default: FlightStatus.Pending,
            enum: Object.values(FlightStatus),
        },
    },
    { timestamps: true }
)

export const FlightsModel = mongoose.model('Flights', schema)
