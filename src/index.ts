import 'reflect-metadata'
require('dotenv').config()

import { createExpressServer } from 'routing-controllers'
import { db } from './memory-database'
import { Request, Response, NextFunction } from 'express'
const port = process.env.PORT

const app = createExpressServer({
    routePrefix: '/v1',
    controllers: [`${__dirname}/controllers/*.controller.*`],
    validation: true,
    classTransformer: true,
    defaultErrorHandler: true,
})

// Connect to In-Memory DB
/* ;(async () => await db({ test: false }))() */

db({ test: false })
    .then(() => {
        app.listen(port, () => {
            console.log(
                `[Live Coding Challenge] Running at http://localhost:${port}`
            )
        })
    })
    .catch((err: unknown) => {
        console.log('Error connecting to Mongo', JSON.stringify(err))
    })

export default app

export class HttpException extends Error {
    readonly message: string
    readonly status: number

    constructor(
        message = 'Server error',
        status: HttpStatus = HttpStatus.ServerError
    ) {
        super(message)
        this.message = message
        this.status = status
    }
}

export enum HttpStatus {
    BadRequest = 400,
    Unauthenticated = 401,
    Unauthorized = 403,
    NotFound = 404,
    ServerError = 500,
}
