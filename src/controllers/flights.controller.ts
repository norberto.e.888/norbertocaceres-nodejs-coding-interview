import { JsonController, Get, Post, Body } from 'routing-controllers'
import {
    FlightsService,
    OnboardPassengerDto,
} from '../services/flights.service'

const flightsService = new FlightsService()

@JsonController('/flights', { transformResponse: false })
export default class FlightsController {
    @Get('')
    async getAll() {
        const flights = await flightsService.getAll()
        return {
            status: 200,
            data: flights,
        }
    }

    @Post('/onboard-passenger')
    async onboardPassenger(@Body() dto: OnboardPassengerDto) {
        const flight = await flightsService.onboardPassenger(dto)
        return {
            status: 200,
            data: flight,
        }
    }
}
